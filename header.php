<!doctype html>
<html <?php language_attributes(); ?>>
<head>    

	<?php include_once( get_template_directory() . '/core/config-header.php'); ?>
	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>


	<?php 
	$class = '';
	if(!is_front_page()){ 
		$class = 'position-sticky';
	}?>
	<div class="header-top <?= $class;?>">
		<a href="<?= site_url();?>">
			<?php
			//Logo customizada do WP
			if ( has_custom_logo() ):
				$image = wp_get_attachment_image_src( get_theme_mod( 'custom_logo' ), 'full' );
				if( false != $image && !is_null( $image ) ): 
					?>
					<img src="<?= $image[0]; ?>"  alt="Seox" title="Seox">
					<?php
				endif;
			endif;
			?>
		</a>
	</div>
