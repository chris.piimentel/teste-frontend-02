<?php

/**
 * Página de 404
 *
 */

get_header();
?>

<div class="page-404">
	<h2>Ops! Página não encontrada.</h2>
	<a href="<?php echo esc_url( home_url( '/' ) ); ?>">IR PARA A PÁGINA INICIAL</a>	
</div>



<?php
get_footer();