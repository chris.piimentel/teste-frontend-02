<?php

get_header();
?>

<div class="container content-blog">
	<?php while ( have_posts() ) : the_post(); ?>

		<!-- Titulo e Resumo -->
		<div class="pt-5">	
			<h1 class="title"><?= the_title();?></h1>
			<div class="py-2 excerpt">
				<?php the_excerpt(); ?>
			</div>
		</div>


		<!-- Informações da publicação e compartilhamento -->
		<div class="header-single-share">
			<div class="col-info bg-gray300 d-flex justify-content-between flex-column flex-md-row">
				<div class="py-2 d-flex align-items-center info-publish">
					<img src="<?= mediaSrc('icon-clock.svg');?>" alt="icone-relogio"> <span><?= get_the_date( 'j/m/Y '); ?> <?= get_the_time(); ?></span>
				</div>
				<?php get_template_part( 'template-parts/share-buttons' ); ?>
			</div>
		</div>


		<!-- Conteúdo do blog e Ads -->
		<div class="row g-5 py-5">
			<div class="col-lg-8">
				<img src="<?= get_the_post_thumbnail_url(get_the_ID(),'full'); ?>" alt="<?= the_title(); ?>" title="<?= the_title(); ?>" class="thumbnail" loading="lazy">
				<div class="py-5 content">
					<?= the_content();?>
				</div>
			</div>
			<div class="col-lg-4 d-flex justify-content-lg-end justify-content-center">			
				<?php get_template_part( 'template-parts/ads-single' ); ?>
			</div>
		</div>


	<?php endwhile; ?>
</div>






<?php
get_footer();
