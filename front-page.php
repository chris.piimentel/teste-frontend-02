<?php
/**
* Template name: Home
*
*/
get_header();
?>

<div class="header-menu">
	<div class="container">
		<div class="col-menu bg-gray800 d-flex justify-content-between flex-column flex-md-row">
			<h2 class="color-red300 text-uppercase">Todos os posts</h2>
			<?php get_template_part( 'template-parts/share-buttons' ); ?>
		</div>
	</div>
</div>

<div class="container py-5">
	<div class="row">
		<div class="col-lg-9">
			<?php
			if( have_posts()):
				while( have_posts() ): the_post();
					?>

					<div class="col-item-blog">
						<a href="<?= get_the_permalink();?>">
							<div class="row">
								<div class="col-lg-4 col-xs-5">
									<img src="<?= get_the_post_thumbnail_url(get_the_ID(),'full'); ?>" alt="<?= the_title(); ?>" title="<?= the_title(); ?>" class="thumbnail" loading="lazy">
								</div>
								<div class="col-lg-8 col-xs-7 d-flex align-items-center">
									<div class="py-3">
										<h3 class="title"><?= the_title(); ?></h3>
										<div class="py-2 d-flex align-items-center info-publish">
											<img src="<?= mediaSrc('icon-clock.svg');?>" loading="lazy" alt="icone-relogio"> <span><?= get_the_date( 'j/m/Y '); ?> - <?= get_the_time(); ?></span>
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

					<?php 
				endwhile;
			endif;  
			wp_reset_postdata(); 
			?> 
		</div>
		<div class="col-lg-3">			
			<?php get_template_part( 'template-parts/ads-home' ); ?>
		</div>
	</div>
</div>



<?php
get_footer();