<div class="share">
	<ul class="a2a_kit">
		<li>
			<a class="social-icons whatsapp a2a_button_whatsapp">
				<img src="<?= mediaSrc('icon-whatsapp.svg');?>" alt="Share Whatsapp"> <span>Compartilhar</span>
			</a>
		</li>
		<li>
			<a class="social-icons telegram a2a_button_telegram">
				<img src="<?= mediaSrc('icon-telegram.svg');?>"  alt="Share Telegram">
			</a>
		</li>
		<li>
			<a class="social-icons facebook a2a_button_facebook">
				<img src="<?= mediaSrc('icon-facebook.svg');?>"  alt="Share Facebook">
			</a>
		</li>
		<li>
			<a class="social-icons twitter a2a_button_twitter">
				<img src="<?= mediaSrc('icon-twitter.svg');?>"  alt="Share Twitter">
			</a>
		</li>
	</ul>
</div>