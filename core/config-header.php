<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="robots" content="index, follow">
<meta name="author" content="Seox">
<link rel="shortcut icon" href="favicon.ico" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="alternate" type="application/rss+xml" href="<?php echo esc_url( home_url( '/' ) ); ?>feed/" title="Feed de <?php wp_title( '/', true, 'right' ); ?>">

<title><?= get_bloginfo('name'); ?></title>


<meta name="description" content="Site desenvolvido pela SEOX - Inteligência digital para Publishers">

<meta name="theme-color" content="#000" />
