<?php

/**
 * Seox (Custom Post Types)
 *
 */


add_action( 'init', 'banners' );
function banners() {

    /**
     * Labels customizados para o tipo de post
     * 
     */
    $labels = array(
	    'name' => _x('Banners', 'post type general name'),
	    'singular_name' => _x('Banners', 'post type singular name'),
	    'add_new' => _x('Adicionar Novo', 'Banner'),
	    'add_new_item' => __('Adicionar Novo Banner'),
	    'edit_item' => __('Editar Banner'),
	    'new_item' => __('Novo Banner'),
	    'all_items' => __('Todos as Banners'),
	    'view_item' => __('Ver Banner'),
	    'search_items' => __('Pesquisar Banner'),
	    'not_found' =>  __('Nenhum Banner Encontrado'),
	    'not_found_in_trash' => __('Nenhum Banner na Lixeira'),
	    'parent_item_colon' => '',
	    'menu_name' => 'Banners'
    );
    
    /**
     * Registamos o tipo de post Projeto através desta função
     * passando-lhe os labels e parâmetros de controle.
     */
    register_post_type( 'banners', array(
	    'labels' => $labels,
	    'public' => true,
	    'publicly_queryable' => true,
	    'show_ui' => true,
	    'show_in_menu' => true,
	    'has_archive' => 'banners',
	    'rewrite' => array(
		 'slug' => 'banners',
		 'with_front' => false,
	    ),
	    'capability_type' => 'post',
	    'has_archive' => true,
	    'hierarchical' => false,
	    'menu_position' => null,
			'menu_icon' => 'dashicons-align-full-width',
	    'supports' => array('title','revisions', 'thumbnail')
	    )
    );
    
}

