<?php
/**
 * Seox (Util)
 *
 */


// Images url
function mediaSrc($image){
    echo get_stylesheet_directory_uri() . '/assets/images/'. $image;
}

//print svg with tags
function get_svg_content($svg){
	return file_get_contents(get_template_directory() . '/assets/images/'. $svg);
}


//Habilitar SVG
function add_file_types_to_uploads($file_types){

    $new_filetypes = array();
    $new_filetypes['svg'] = 'image/svg+xml';
    $file_types = array_merge($file_types, $new_filetypes );

    return $file_types;
}
add_action('upload_mimes', 'add_file_types_to_uploads');


?>