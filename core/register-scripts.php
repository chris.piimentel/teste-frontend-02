<?php

/**
 * Seox (Register Scripts)
 *
 */
function scripts()
{

    $utilsVersion = "1.0";


    $theme_version = wp_get_theme()->get('Version');

    // CSS
    wp_enqueue_style('bootstrap-css', get_template_directory_uri() . '/assets/css/custom-style.min.css?=v'.time(), array(), $theme_version);

    // Scripts
    wp_enqueue_script('jquery', get_template_directory_uri() . '/assets/js/jquery.js?=v'.time(), array(), $theme_version, true);
    wp_enqueue_script('scripts', get_template_directory_uri() . '/assets/js/scripts.min.js?=v'.time(), array(), $theme_version, true);
}
add_action('wp_enqueue_scripts', 'scripts');
?>